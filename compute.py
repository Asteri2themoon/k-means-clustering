from tkinter import *
import csv
import numpy as np

colors=['red','blue','green','yellow','cyan','pink','purple','navy','magenta','gold']

k=int(input("k:"))

points=np.loadtxt(open("points.csv", "rb"), delimiter=",")
classP=np.zeros(len(points), dtype=np.int)
c=np.random.randint(800, size=(k, 2))
	
def display():
	canvas.delete("all")
	for i in range(len(points)):
		canvas.create_oval(int(points[i][0]-3),int(points[i][1]-3),int(points[i][0]+3),int(points[i][1]+3),fill=colors[classP[i]])
	for i in range(len(c)):
		canvas.create_oval(int(c[i][0]-6),int(c[i][1]-6),int(c[i][0]+6),int(c[i][1]+6),fill=colors[i])

def next():
	for i in range(len(points)):
		min,pos=1e9,0
		for j in range(len(c)):
			d = np.linalg.norm(c[j]-points[i])
			if d<min:
				min=d
				pos=j
		classP[i]=pos
	
	for i in range(len(c)):
		x,y,count=0,0,0
		for j in range(len(points)):
			if classP[j]==i:
				x+=points[j][0]
				y+=points[j][1]
				count+=1
		if count!=0:
			c[i]=[x/count,y/count]
	display()

fenetre=Tk()

canvas = Canvas(fenetre, width=800, height=800, background='white')
canvas.focus_set()
canvas.pack()
bouton=Button(fenetre, text="suivant", command=next)
bouton.pack()
display()
fenetre.mainloop()