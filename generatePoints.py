from tkinter import *
import numpy

points=[]

def save():
	print("save")
	#print(points)
	numpy.savetxt("points.csv", numpy.asarray(points), delimiter=",")

def mouse(event):
	x, y = event.x, event.y
	points.append([x,y])
	canvas.create_oval(x-3,y-3,x+3,y+3,fill='black')

fenetre =Tk()

canvas = Canvas(fenetre, width=800, height=800, background='white')
canvas.focus_set()
canvas.bind("<Button-1>", mouse)
canvas.pack()
bouton=Button(fenetre, text="enregistrer", command=save)
bouton.pack()
fenetre.mainloop()